extends Node

var current = null
#var drag_offset = Vector2(0,0) #drag_offset abandonned in favor of card center
var candidates = []
export var drag_group = "draggable"

onready var touchCam = get_node("/root/Game/TouchCamera")


func _process(_delta):
	if current is Node2D:
#		current.global_position = current.get_global_mouse_position() - drag_offset
		current.global_position = current.get_global_mouse_position() - current.get_rect().size/2

func connect_signals_to(target: Object,parameters: Array):
	if not has_method("mouse_entered"):
		target.connect("mouse_entered",self,"mouse_entered",parameters)
	if not has_method("mouse_exited"):
		target.connect("mouse_exited",self,"mouse_exited",parameters)
	if not has_method("input_event"):
		target.connect("input_event",self,"input_event",parameters)

func _ready():
	var dragbag = get_tree().get_nodes_in_group(drag_group)
	for d in dragbag:
		for child in d.get_children():
			
			if child is CollisionObject2D:
				connect_signals_to(child,[d])

func mouse_entered(which):
	print("appended ",which.name," to candidates")
	candidates.append(which)

func mouse_exited(which):
	print("erased ",which.name," from candidates")
	candidates.erase(which)


func input_event(_viewport: Node, event: InputEvent, _shape_idx: int, which:Node2D):
	if event is InputEventScreenTouch and which.is_in_group(drag_group):
		if event.is_pressed():
			drag()
		else:
			drop()

func drag():
	#shut down the camera to prioritize dragndrop
	if touchCam != null: touchCam.active = false
	
	var candinames = []
	for c in candidates:
		candinames.append(c.name)
	print("candidates to dragging are ",candinames)
	
	candidates.sort_custom(self,"depth_sort")
	var last = candidates.back()
	if last != null:
		last.raise()
		current = last
#		drag_offset = current.get_global_mouse_position() - current.global_position
		if current.has_method("_on_drag_start"):
			current._on_drag_start()
		print(current.name,"is being dragged")

func drop():
	var can_drop = true
	if current:
		if current.has_method("_on_drop"):
			var on_drop_result = current._on_drop()
			can_drop = on_drop_result == null || on_drop_result
		print(current.name," is dropped")
		if can_drop:
			current = null
	
	#restore the camera
	if touchCam != null: touchCam.active = true


func depth_sort(a,b):
	return b.get_index()<a.get_index()

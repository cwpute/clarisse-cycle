extends Node

var debug_group = "debug"
onready var nodebag = get_tree().get_nodes_in_group(debug_group)

func _ready():
	for n in get_tree().get_nodes_in_group("debug"):
		n.set_visible(false)

func _input(event):
	if event.is_action_pressed("debug_visible"):
		for n in nodebag:
			if n.has_method("set_visible()"):
				if n.visible: n.visible = false
				else: n.visible = true
			print("is debug visible ?",n.visible)
	
	if event.is_action_pressed("debug_gameover"):
		get_node("/root/Singleton").end_game("debug")

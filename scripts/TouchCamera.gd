extends Camera2D

export (NodePath) var target

# Optional: export these properties for convenient editing.

var target_return_enabled = true
var target_return_rate = 0.02
var min_zoom = 0.5
var max_zoom = 2
var zoom_sensitivity = 10
var zoom_speed = 0.05

var active := true

var events = {}
var last_drag_distance = 0

#should pinpoint the center of the 4 cards and place the camera right there at the start.
#func _ready():
#	global_position = (get_viewport_rect().size/2)-Vector2(200,200)

func _process(_delta):
	if target and target_return_enabled and events.size() == 0:
		position = lerp(position, get_node(target).position, target_return_rate)

func _unhandled_input(event):
	if event is InputEventScreenTouch:
		if event.pressed:
			events[event.index] = event
		else:
			events.erase(event.index)
	if event is InputEventScreenDrag:
		events[event.index] = event
		if events.size() == 1 and active:
			position -= event.relative.rotated(rotation) * zoom.x
		elif events.size() == 2:
			var drag_distance = events[0].position.distance_to(events[1].position)
			if abs(drag_distance - last_drag_distance) > zoom_sensitivity:
				var new_zoom = (1 + zoom_speed) if drag_distance < last_drag_distance else (1 - zoom_speed)
				new_zoom = clamp(zoom.x * new_zoom, min_zoom, max_zoom)
				zoom = Vector2.ONE * new_zoom
				last_drag_distance = drag_distance
	
	
	if Input.is_action_pressed("camera_zoom"):
		if zoom == Vector2(.5,.5):
			print("!! Can't zoom, hit limit")
		else: set_zoom(zoom-Vector2(.5,.5))
	
	if Input.is_action_pressed("camera_dezoom"):
		if zoom == Vector2(3,3):
			print("!! Can't dezoom, limit hit")
		else: set_zoom(zoom+Vector2(.5,.5))

extends Sprite
class_name BCard

const matTextures = [preload("res://resources/stone200x200.png"),
preload("res://resources/wood200x200.png"),
preload("res://resources/metal200x200.png"),
preload("res://resources/glass200x200.png"),
preload("res://resources/fallback200x200.png")] #fallback should always be last in array

onready var nHand = get_node("/root/Game/PlayerGUI/MarginContainer/HBoxContainer/PlayerHand")
onready var nBCards = get_node("/root/Game/PlayerGUI/MarginContainer/HBoxContainer/PlayerHand/BCards")
onready var nGrid = get_node("/root/Game/Grid")
onready var nHandButton = get_node("/root/Game/PlayerGUI/MarginContainer/HBoxContainer/PlayerHand/HandButton")

export var value := -1
export var suit := -1
export var debris := false

#quick parameters (qp) array designed for passing cards parameters outside of self
var qp := Vector2(value,suit)

var oldpos : Vector2
#var overHand : bool
#var firstPress := true


func _ready():
	position = -get_rect().size/2
#	if get_parent() == Grid : overHand = false
#	else: overHand = true
	oldpos = global_position
#	update_param()


func update():
	qp = Vector2(value,suit)
	set_texture(matTextures[suit])
	$Label.set_text(String(value))
	name = "Card%s" % String(qp)
	if debris: turn_to_debris()
	else:
		DragDropController.connect_signals_to($Area2D,[self])
		turn_to_building()
	
	if get_parent() == nBCards :
		if not has_method("_on_HandButton_mouse_entered()"):
			nHandButton.connect("mouse_entered",self,"_on_HandButton_mouse_entered")
		if not has_method("_on_HandButton_mouse_exited()"):
			nHandButton.connect("mouse_exited",self,"_on_HandButton_mouse_exited")
	print(name," updated")


func turn_to_debris():
	debris = true
	set_modulate(Color(0.4, 0.4, 0.4, 0.631373))
	$Label.visible = false
	flip_h = true
	flip_v = true
#	if is_in_group("draggable"): remove_from_group("draggable")

func turn_to_building():
	debris = false
	set_modulate(Color(1,1,1,1))
	$Label.visible = true
	flip_h = false
	flip_v = false
#	if not is_in_group("draggable") and Hand.topmost == self: add_to_group("draggable")

func _on_drag_start():
	oldpos = global_position
	if get_parent() != nGrid:
		get_parent().remove_child(self)
		nGrid.add_child(self)
		
	#++check for and highlight available positions on the grid
	#++show recycle button

func _on_drop():
	print(".. Applying drop function to ",name," ..")
	
	#set in grid if not in hand
#	if not overHand:
	if true:
		print("is out of hand area")
		#remove from dragdrop candidates
		if DragDropController.candidates.has(self) : DragDropController.candidates.erase(self)
		
		#reparent and set in grid
		if not nGrid.set_in_grid(self): nGrid.return_to_hand(self)
		
		#update hand
		nHand.update()
		
	#return to old pos if over hand area
	else:
		print("is in hand area")
		global_position = oldpos
		print("returned to its original pos")
	
	print(".. drop function ended")


func _on_BCard_tree_entered():
	update()
	print(name," entered tree in ",get_parent().name)


func _on_HandButton_mouse_entered():
#	print("mouse entered button")
#	overHand = true
	pass

func _on_HandButton_mouse_exited():
#	print("mouse exited button")
#	overHand = false
	pass

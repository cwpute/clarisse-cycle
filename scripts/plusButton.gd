extends Button

var ev = InputEventAction.new()

func _ready():
	ev.action = "camera_zoom"
	ev.pressed = true

func _on_Button_pressed():
	Input.parse_input_event(ev)

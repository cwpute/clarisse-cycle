extends Button


onready var ddcurrent = DragDropController.current
onready var Hand = get_parent()


func _on_Hand_ready():
	set_position(Hand.topmost.position)
	rect_size = Hand.topmost.texture.get_size()


func _on_HandButton_button_down():
	print("button down, timer started")
	$Label/Timer.start()
	DragDropController.mouse_entered(Hand.topmost)

func _on_Timer_timeout():
	print("timer timeout")
#	if pressed and Hand.topmost.overHand: DragDropController.drag()
	if pressed: DragDropController.drag()
	else: DragDropController.drop()

func _on_HandButton_button_up():
	$Label/Timer.stop()
	DragDropController.drop()
	if Hand.topmost:
		DragDropController.mouse_exited(Hand.topmost)
		Hand.set_draggable()


func _on_ButtonArea2D_area_entered(area):
	if area.get_parent() == Hand.topmost:
		print("topmost ",area.get_parent().name," entered button area2D")
#		Hand.topmost.overHand = true

func _on_ButtonArea2D_area_exited(area):
	if area.get_parent() == Hand.topmost:
		print("topmost ",area.get_parent().name," exited button area2D")
#		Hand.topmost.overHand = false

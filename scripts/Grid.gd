class_name Grid
extends Node2D

#Used to keep track and position objects placed on a grid
#objects placed on the grid must be children of the Grid node

#Cell size in pixels
export var cell_size : Vector2
#Grid size in rows and columns of cells
export var size : Vector2

onready var Hand = get_node("/root/Game/PlayerGUI/MarginContainer/HBoxContainer/PlayerHand")
onready var BCards = get_node("/root/Game/PlayerGUI/MarginContainer/HBoxContainer/PlayerHand/BCards")
var cardScene = load("res://scenes/BCard.tscn")
var onGrid = []



func _ready():
	setup_newgame()

func setup_newgame():
	print(".. New game setup started ..")
	
	#remove any current card in grid
	for c in get_children():
		remove_child(c)
		c.queue_free()
	
	#calculate array of positions
	var poslist=[]
	for x in [0,1]:
		for y in [0,1]:
			poslist.append(Vector2(x,y))
	
	#instance new cards in Grid
	for card in Singleton.draw(4,[false],true):
		var newcard = cardScene.instance()
	#	newcard.owner = self #only useful when saving as PackedScene (??)
		newcard.value = card[0]; newcard.suit = card[1]; newcard.debris = true
		newcard.add_to_group("setup")
		add_child(newcard)
		if set_in_grid(newcard): newcard.global_position = grid_to_global(poslist.pop_back())
		print("newcard ",newcard.name," set to pos ",newcard.global_position)
	
	print(".. new game setup ended")

#calculates vector position in the grid, clamped in the grid
func gridize(pos: Vector2) -> Vector2:
	return (pos/cell_size).round()

#calculates global vector position, clamped in the grid
func degridize(pos: Vector2) -> Vector2:
	return (pos/cell_size).round()*cell_size


#func printpos(card: Object):
#	print("relatpos=",card.position," / globalpos=",card.global_position," / relatgridpos=",global_to_grid(card.position)," / globalgridpos=",global_to_grid(card.global_position))
#	print("localmousepos=",get_local_mouse_position()," / globalmousepos=",get_global_mouse_position()," / localmousegridpos=",global_to_grid(get_local_mouse_position())," / globalmousegridpos=",global_to_grid(get_global_mouse_position()))
#	print("IN GRID :: relatgridpos=",gridize(card.position),", globalgridpos=",gridize(card.global_position))
#	print("IN GRID :: localmousegridpos=",gridize(get_local_mouse_position())," / globalmousegridpos=",gridize(get_global_mouse_position()))


func set_in_grid(card: Object) -> bool:
	print(".. Setting up ",card.name," in grid ..")
	
#	printpos(card)
	
	if is_in_gridarray(card):
		print(card.name," is already in the grid")
		return false
	else:
		#calculate new gridpos
		var futuregridpos = gridize(card.get_global_position())
		
		#check for game rules
		if card.is_in_group("setup") or is_correct_gridpos_for(futuregridpos,card):
			
			#add card as a child of grid
			if card.get_parent() != self:
				card.get_parent().remove_child(card)
				add_child(card)
				print("is now a child of grid")
			
			#set card position
#			card.position = Vector2(0,0)
			card.global_position = grid_to_global(futuregridpos)
			print("position changed to ",grid_to_global(futuregridpos),", knwon as ",futuregridpos," in the grid")
			
			#check for debris at the same gridpos and return them to hand
			for n in onGrid:
				if not n.debris:
					continue
				if n != card and n.global_position==card.global_position:
					return_to_hand(n)
			
			#remove from draggable group
			if card.is_in_group("draggable"):
				card.remove_from_group("draggable")
				print("removed from draggable")
			
			#add card to the onGrid array for later identification
			if not onGrid.has(card):
				onGrid.append(card)
				print("appended to onGrid array")
			
#			return true
		
		#if cant place card, return it to its original position
		else:
			return_to_hand(card)
			print("couldn't set in grid: returned to hand")
			
			print(".. setup ended.")
			return false
		
		card.z_index = 0
		
#		printpos(card)
	
	print(".. setup ended.")
	return true

#perhaps move this function to the singleton ?
func return_to_hand(card: Object):
	print(".. Returning ",card.name," to player hand ..")
	if card.get_parent() == self:
		if card.is_in_group("setup"): card.remove_from_group("setup")
		remove_child(card)
		if onGrid.has(card): onGrid.erase(card)
		BCards.add_child(card)
		card.turn_to_building()
		Hand.update()
		print(".. returned card to player hand")
	else: print(".. didn't return ",card.name," to hand: not a child of grid")

#func is_in_grid(globalpos: Vector2) -> bool:
#	if step_decimals(global_to_grid(globalpos).x)==0 and step_decimals(global_to_grid(globalpos).y)==0:
#		return true
#	else: return false


func get_neighbours(gridpos: Vector2) -> Array:
	#show all card positions on grid
	var ongridpos = []
	var neighpos = []
	for c in onGrid:
		ongridpos.append(global_to_grid(c.global_position))
	
	#compare gridpos of target to each on grid
	var neighbours = []
	
	#check for direct neighbours
	for card in onGrid:
		var cgrpos = global_to_grid(card.global_position)
		for x in [-1, 0, 1]:
			#check if child is at that pos
			if cgrpos == gridpos+Vector2(0,x) or cgrpos == gridpos+Vector2(x,0):
				neighbours.append(card)
				neighpos.append(cgrpos)
	
	print("among ",ongridpos,", neighbours to ",gridpos," are ",neighpos)
	return neighbours

#checks for rule-breaking when setting up a card in the grid
func is_correct_gridpos_for(gridpos: Vector2,card) -> bool:
	print(".. Checking if ",gridpos," is a correct gridpos for ",card.name," ..")
	var neighbours = get_neighbours(gridpos)
	
	#card cant be placed next to nothing or on top of nothing
	#if there are no buildings nearby, then there are no neighbours
	if neighbours.empty():
		print("can't place here: no neighbours")
		return false
#	for n in neighbours:
#		if not n.debris:
#			print("can't place here: no neighbouring buildings")
#			return false
	
	var debrisatcardpos = false
	var buildingsaround = false
	for n in neighbours:
		#checking for neighbouring debris
		if n.debris:
			if gridpos == global_to_grid(n.global_position): #debris at card pos is accoutned for
				debrisatcardpos = true
				continue
			else: continue #debris around the card pos are ignored
		else: buildingsaround = true
		
		#cards can't be placed where a building is already
		if gridpos == global_to_grid(n.global_position):
			print(".. can't place ",card.name," at ",gridpos," : ",n.name," is at the same postion ",global_to_grid(n.global_position)," and is not a debris")
			return false
		
		#check for neighbouring building suits
		if card.suit == n.suit:
			print(".. can't place here: ",n.name," suit is the same")
			return false
		
		#check for neighbouring building values
		var vresult := false
		for d in [-1,+1]:
#			print("is ",card.name," value close enough to ",n.value," ? checking with card.value+d:",card.value+d," (with d=",d,")")
			if card.value+d == n.value:
				vresult = true
				break
		if vresult == false:
				print(".. can't place here: ",n.name," difference in value to future ",card.name," isn't of exactly 1 point")
				return false
	
	if not debrisatcardpos and not buildingsaround:
		print(".. can't place here: no debris under future card pos and no buildings around")
		return false
	
	
	print(".. ",gridpos," is a correct gridpos for ",card.name)
	return true


func is_in_gridarray(card: Object) -> bool:
	if onGrid.has(card): return true
	else: return false

#turns grid position to global position
func grid_to_global(pos: Vector2) -> Vector2:
	return pos*cell_size

#turns global position to grid position
func global_to_grid(pos: Vector2) -> Vector2:
	return pos/cell_size

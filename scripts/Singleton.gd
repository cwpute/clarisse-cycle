extends Node

#Defines the buildings still available in the deck
#Stores card data

export var maxCardsInHand = 4
export var max_value = 8
export var max_materials = 4
export var mixedDrawDeck = false

#Cards will be arrays set as such:
#[value,material]

var fullDeck = []
var discarded = []

var sDeck = []; var wDeck = []; var mDeck = []; var gDeck = []

var sType = {"id": 0, "name": "Stone", "array": sDeck}
var wType = {"id": 1, "name": "Wood", "array": wDeck}
var mType = {"id": 2, "name": "Metal", "array": mDeck}
var gType = {"id": 3, "name": "Glass", "array": gDeck}

var materials = [sType,wType,mType,gType]

var playerScore := 0
var playerRounds :=0


func _ready():
	randomize()
	build_decks()


func build_decks():
	
	for m in max_materials:
		for v in max_value:
			if v==0: continue
			materials[m]["array"].append([v,materials[m]["id"]])
		materials[m]["array"].shuffle()
	
	if mixedDrawDeck :
		fullDeck.append_array(sDeck,mDeck,gDeck,wDeck)
		fullDeck.shuffle()
		sDeck.clear()
		mDeck.clear()
		gDeck.clear()
		wDeck.clear()


func discard_from_game(cardqp: Vector2):
	discarded.append(cardqp)
	if mixedDrawDeck: fullDeck.erase(cardqp)
	else:
		materials[cardqp.y]["array"].erase(cardqp)


func is_discarded(cardqp: Vector2):
	if discarded.has(cardqp): return true
	else: return false


func draw(number: int = maxCardsInHand, fromDeck: Array = [false], one_of_each: bool = false) -> Array:
	var drawn_cards = []
	
	if fromDeck.front() != false:
		for i in number:
			if fromDeck.front() != null: drawn_cards.append(fromDeck.pop_front())
			else:
				end_game("fromDeck cards are being drawn from is empty")
				print("fromDeck is empty")
				break
	elif mixedDrawDeck:
		for i in number:
			if not fullDeck.empty(): drawn_cards.append(fullDeck.pop_front())
			else:
				end_game("Main draw deck is empty")
				print("fullDeck is empty")
				break
	elif one_of_each:
		var emptydecks:=0
		for i in max_materials:
			if not materials[i]["array"].empty(): drawn_cards.append(materials[i]["array"].pop_front())
			else:
				emptydecks+=1
				print(materials[i]["name"]," deck is empty")
		if emptydecks == max_materials: end_game("all decks empty")
	else:
		#should prompt player with different decks to choose from
		#one of each instead for now
		var emptydecks:=0
		for i in max_materials:
			if not materials[i]["array"].empty(): drawn_cards.append(materials[i]["array"].pop_front())
			else:
				emptydecks+=1
				print(materials[i]["name"]," deck is empty")
		if emptydecks == max_materials: end_game("all decks empty")
#		print("not enough deck drawing rules defined: no draw")
	
	if drawn_cards.empty():
		print("no cards drawn")
		#proceed to end the game
	else:
		drawn_cards.shuffle()
		print("cards drawn: ",drawn_cards)
	
	#returns an array of card ids arrays
	return drawn_cards


func recycle(cardqp: Vector2):
	print("Recycling..")
	materials[cardqp.y]["array"].append(cardqp)
	return materials[cardqp.y].pop_front()

func end_round():
	var points := 0
	
	for c in get_node("/root/Game/Grid").onGrid:
		if not c.debris:
			points +=1 #each building on the grid grants a single point
			c.turn_to_debris() #each building is turned into debris
	playerScore += points
	
	playerRounds += 1
	print(points," points this round : player score is now ",playerScore)

func end_game(reason: String):
	get_node("/root/Game/GameOverTop/GameOverScreen").visible = true
#	get_node("root/Game/PlayerGUI/MarginContainer/HBoxContainer/DecksButtons/HelpContainer").visible = false
#	get_node("root/Game/PlayerGUI/MarginContainer/HBoxContainer/DecksButtons/RecycleContainer").visible = false
#	get_node("root/Game/PlayerGUI/MarginContainer/HBoxContainer/DecksButtons/NextRoundContainer").visible = false
#	get_node("root/Game/PlayerGUI/MarginContainer/HBoxContainer/PlayerHand").visible = false
	
	print("--GAME OVER-- ",reason)

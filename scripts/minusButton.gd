extends Button

var ev = InputEventAction.new()

func _ready():
	ev.action = "camera_dezoom"
	ev.pressed = true

func _on_Button_button_down():
	Input.parse_input_event(ev)

extends Tween

onready var handB = get_parent().get_node("HandButton")
onready var BCards = get_parent().get_node("BCards")


func _on_HandButton_pressed():
	handB.disabled = true
	#cycle through cards
	var next
	for card in BCards.get_children():
		if card.get_index()+1 != BCards.get_child_count() :
			next = BCards.get_child(card.get_index()+1)
			interpolate_property(card, "transform", card.transform, next.transform, 0.5, 5, 1, 0)
			start()
			interpolate_property(card, "z_index", card.z_index, next.z_index, 0, 5, 1, 0)
			start()
		else:
			next = BCards.get_child(0)
			interpolate_property(card, "transform", card.transform, next.transform, 0.5, 5, 1, 0)
			start()
			interpolate_property(card, "z_index", card.z_index, next.z_index, 0, 5, 1, 0)
			start()
	#draggable state is restored in signal function after tweens are all completed

func _on_Tween_tween_all_completed():
	get_parent().set_draggable()
	handB.disabled = false

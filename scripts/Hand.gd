extends Node2D

var cardScene = preload("res://scenes/BCard.tscn")
export var Backmost_pos := Vector2(0,0)
export var Topmost_pos := Vector2(100,100)
var topmost: Object


func _ready():
	#set up hand for the first time
	setup_hand(true)


func setup_hand(first_draw : bool = false):
	
	#discard previous cards
	var cardsInHand = $BCards.get_children()
	if not cardsInHand.empty() :
		for c in cardsInHand:
			if not first_draw: Singleton.discard_from_game(c.qp)
			$BCards.remove_child(c)
			c.queue_free()
			DragDropController.candidates.erase(c)
	else: print("cannot empty hand: hand is already empty")
	
	#draw new cards
	var futurecards = Singleton.draw(Singleton.maxCardsInHand,[false],first_draw)
	if futurecards.empty():
		print("cannot set hand: no cards drawn")
		return
	
	#instance those new cards
	for card in futurecards:
		var newcard = cardScene.instance()
#		newcard.owner = self #only useful when saving as PackedScene (??)
		newcard.value = card[0]; newcard.suit = card[1]
		$BCards.add_child(newcard)
	
	#set cards to new position in hand
	update()

func update():
	if not $BCards.get_children().empty():
		set_positions()
		set_draggable()
	else: print("hand is empty: card positions and draggable stat not updated")

#func calculate_positions(cards_count: int, Backmost_pos: Vector2, Topmost_pos: Vector2):
#	#deal with special cases
#	if cards_count==1: return [Topmost_pos]
#	elif cards_count==2: return [Backmost_pos,Topmost_pos]
#
#	var array = []
#	var step = (Backmost_pos-Topmost_pos)/(cards_count-1)
#	for c in cards_count: array.append(Backmost_pos+step*c)
#
#	#returns an array of positions, from back card to top card
#	return array 


func set_positions():
	var cards_array = $BCards.get_children()
	
	#calculate an array of positions
	var pos_array = [] #array of positions, from back card to top card
	#special cases
	var cards_count = $BCards.get_child_count()
	if cards_count==1: pos_array = [Topmost_pos]
	elif cards_count==2: pos_array = [Backmost_pos,Topmost_pos]
	#usual cases
	else:
		var step = (Topmost_pos-Backmost_pos)/(cards_count-1)
		for c in cards_count:
			pos_array.append(Backmost_pos+step*c)
	
	#apply array of positions + index level to the array of cards
	var i=0
	for c in cards_array:
		c.position.x = pos_array[i].x; c.position.y = pos_array[i].y
		c.z_index = i
		topmost = c
		i+=1


func set_draggable():
	
	#sort hand cards from lowest to highest z-index
	var sorted = $BCards.get_children()
	sorted.sort_custom(self,"depth_sort")
	topmost = sorted.front()
	
	#assign or remove draggable propriety
	for card in sorted:
		if card == topmost:
			if not card.is_in_group("draggable"): card.add_to_group("draggable")
		elif card.is_in_group("draggable"):
			card.remove_from_group("draggable")
	
	var dragzat = []
	var dragnot = [] 
	for c in sorted:
		if c.is_in_group("draggable"): dragzat.append(c.name)
		else: dragnot.append(c.name)
	print(dragzat," is topmost, hence draggable // ",dragnot," are not draggable")


func depth_sort(a,b):
	if b.get_z_index()<a.get_z_index(): return true
	else: return false


func _on_RecycleButton_pressed():
	$"../DecksButtons/RecycleContainer/RecycleTexture/AnimationPlayer".play("rotate")

func _on_NextRoundButton_pressed():
	$"../DecksButtons/NextRoundContainer/TextureRect/AnimationPlayer".play("next turn")
	#score updated and buildings turned into debris
	Singleton.end_round()
	#new cards drawn, old cards discarded
	setup_hand()

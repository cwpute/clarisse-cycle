extends CollisionShape2D

func _on_Hand_ready():
	var topcard = get_node("../..").topmost
	set_position(topcard.texture.get_size()/2)
	shape.extents = topcard.texture.get_size()/2
